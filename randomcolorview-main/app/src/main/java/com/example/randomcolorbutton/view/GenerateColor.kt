package com.example.randomcolorbutton.view

import android.graphics.drawable.ClipDrawable.HORIZONTAL
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridLayout.HORIZONTAL
import android.widget.LinearLayout.HORIZONTAL
import androidx.appcompat.widget.LinearLayoutCompat.HORIZONTAL
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.randomcolorbutton.Resource
import com.example.randomcolorbutton.databinding.FragmentGenerateColorBinding
import com.example.randomcolorbutton.view.adapter.ColorAdapter
import com.example.randomcolorbutton.view.adapter.FillBackgroundAdapter
import com.example.randomcolorbutton.view.adapter.HorizontalAdapter
import com.example.randomcolorbutton.view.adapter.OtherColorAdapter
import com.example.randomcolorbutton.viewmodel.MainViewModel

class GenerateColor : Fragment() {
    private var _binding: FragmentGenerateColorBinding? = null
    private val binding: FragmentGenerateColorBinding get() = _binding!!
    var clicked : Boolean = false

    private val viewmodel by viewModels<MainViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentGenerateColorBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()
        initListeners()
    }

    private fun initListeners() {

            viewmodel.getColors()

    }

    private fun initObservers() = with(viewmodel) {
        color.observe(viewLifecycleOwner){ viewState ->
            when(viewState){
                Resource.Loading -> {
                    // Circular progress spinning still
                }
                is Resource.Success -> {
                    binding.recycleViewFun.layoutManager = LinearLayoutManager(requireContext())
                    binding.recycleOtherColors.layoutManager = LinearLayoutManager(requireContext())
                    binding.annotherRecycleColors.layoutManager = LinearLayoutManager (requireContext() )
                    binding.horizontal.layoutManager = LinearLayoutManager(requireContext(),LinearLayoutManager.HORIZONTAL,false)

                    binding.recycleViewFun.adapter = ColorAdapter().apply {
                        giveData(viewState.data)
                    }
                    binding.button5.setOnClickListener {
                        viewmodel.getColors()

                        binding.recycleOtherColors.adapter = OtherColorAdapter().apply {
                        getData(viewState.data)
                    }

                    }

                    binding.button6.setOnClickListener {
                        if (clicked == false){
                            binding.annotherRecycleColors.visibility =View.VISIBLE
                            binding.horizontal.visibility = View.GONE


                            viewmodel.getColors()

                        binding.annotherRecycleColors.adapter = FillBackgroundAdapter().apply {
                            senData(viewState.data)
                            clicked = true

                        }
                        }
                        else{
                            binding.annotherRecycleColors.visibility = View.GONE
                            binding.horizontal.visibility = View.VISIBLE

                            viewmodel.getColors()
                            binding.horizontal.adapter = HorizontalAdapter().apply {
                                horizdata(viewState.data)
                            }
                            clicked = false

                        }


                    }

                }
            }
        }
    }
}
