package com.example.randomcolorbutton.view.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.randomcolorbutton.R
import com.example.randomcolorbutton.databinding.*

class HorizontalAdapter : RecyclerView.Adapter<HorizontalAdapter.LovelyViewholder>() {

    private lateinit var data: List<Int>

    class LovelyViewholder(
        private val binding: HorizontalBinding

    ) : RecyclerView.ViewHolder(binding.root) {
        fun apply(item: Int) {

            binding.horizontal.setCardBackgroundColor(item)

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LovelyViewholder {
        val binding = HorizontalBinding.inflate(LayoutInflater.from(parent.context))
        return LovelyViewholder(binding)
    }

    override fun onBindViewHolder(holder: LovelyViewholder, position: Int) {
        val item = data[position]
        holder.apply(item)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun horizdata(data: List<Int>){
        this.data = data
    }
}