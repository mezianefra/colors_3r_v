package com.example.randomcolorbutton.viewmodel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.randomcolorbutton.Resource
import com.example.randomcolorbutton.model.Repo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {

    private val repo by lazy { Repo }

    private val _color: MutableLiveData<Resource> = MutableLiveData(Resource.Loading)
    val color: LiveData<Resource> get() = _color

    fun getColors() {
        viewModelScope.launch(Dispatchers.Main) {
            val response = repo.getdata()
            _color.value = response
        }
    }
}